# Reespirator

* [Organización de grupo](ORGANIZACION.md) (subequipos y tareas asignadas/pendientes)
* [Manejo del repositorio git](GIT.md) (issues, ramas, etiquetas, merges)
* [FAQ](FAQ.md) (preguntas asiduamente formuladas y sus respuestas)
* [Documentación y hardware Reespirator](https://gitlab.com/coronavirusmakers/reespirator-doc)
* [Propuesta protocolo para monitor](https://docs.google.com/document/d/1lItbWZhYFjCUJKEzwG3V0N3ZbFNCW4r7WvXlSnQcjlk/edit#heading=h.xgx60y6l5inf) (Descripción Comunicaciones Serie Externas Arduino Reespirator)

## Otros recursos útiles

* [Arduino Reference](https://www.arduino.cc/reference/en/)
* [Arduino Best Practices and Gotchas](https://www.theatreofnoise.com/2017/05/arduino-ide-best-practices-and-gotchas.html)
* [C++ Best Practices (Gitbook)](https://lefticus.gitbooks.io/cpp-best-practices/content/)
* [Notas básicas sobre uso de PlatformIO](https://docs.google.com/document/d/1kwAkf1QT0JX9wEM3eoz8DvTOIxe7He3aKWrSH5j_k6A/edit?usp=sharing)
* [Atmega2560 Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf)
